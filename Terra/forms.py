from django import forms

class Status_Form(forms.Form):
    error_messages = {
          "required": "This field is required",
          "invalid": "is not valid",
          }

    title_attrs = {
        "type" : "text",
        "class": "form-control",
        "placeholder" : "Input title",
        }

    message_attrs = {
        "type" : "text",
        "class": "form-control",
        "placeholder" : "Input status",
        }
        
    title = forms.CharField(label = "title", required = True, max_length = 100,
    widget = forms.TextInput(attrs = title_attrs))
    message = forms.CharField(label = "status", required = True, max_length = 300,
    widget = forms.TextInput(attrs = message_attrs))