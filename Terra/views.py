from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from .forms import Status_Form
from .models import Status

# Create your views here.

back_img = "/static/image/Background.jpg"

response = {}

def index(request):
    response["back_img"] = back_img
    response["form"] = Status_Form
    response["final"] = Status.objects.all()
    return render(request, 'index.html', response)

def post_status(request):
    form = Status_Form(request.POST or None)
    if(request.method == "POST" and form.is_valid()):
        response = request.POST
        Status(title= response["title"], message = response["message"]).save()
    return redirect(index)
