# Generated by Django 2.1.1 on 2018-11-01 04:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Terra', '0002_auto_20181031_1259'),
    ]

    operations = [
        migrations.RenameField(
            model_name='status',
            old_name='text',
            new_name='message',
        ),
        migrations.AddField(
            model_name='status',
            name='title',
            field=models.CharField(default='a', max_length=100),
            preserve_default=False,
        ),
    ]
