from django.urls import path
from django.conf.urls import url
from Terra import views
from .views import index, post_status

urlpatterns = [
    path(r'index/', index, name="index"),
    url(r'^$', index, name="index"),
    url(r'^post_status/$', post_status, name='post_status'), 
]