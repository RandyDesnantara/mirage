from django.test import TestCase, Client
from django.urls import resolve
from selenium.webdriver.common.keys import Keys
from .views import index, post_status
import time
import unittest

# Create your tests here.
class MyPageTest(TestCase):
    def test_Terra_url_is_exist(self):
        response = Client().get("")
        self.assertEqual(response.status_code, 200)

    def test_Terra_using_base_template(self):
        response = Client().get("")
        self.assertTemplateUsed(response, "base.html")

    def test_Terra_using_index_func(self):
        found = resolve("")
        self.assertEqual(found.func, index)

    """ def test_Terra_post_success_and_redirect(self):
        test = "Fasilkom"
        response_post = Client().post("", data={"title": test, "message": test })
        self.assertEqual(response_post.status_code, 200)

        response = Client().get("")
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)
 """
